- Getting started

  - [mermaid](README.md)
  - [FAQ](faq.md)
  - [Usage](usage.md)
  - [Demos](demos.md)
  - [mermaid CLI](mermaidCLI.md)

- Diagrams

  - [Flowchart](flowchart.md)
  - [Sequence diagram](sequenceDiagram.md)
  - [Gantt](gantt.md)

- Guide

  - [Development](development.md)
  - [mermaidAPI](mermaidAPI.md)
  - [Vue compatibility](vue.md)
  - [CDN](cdn.md)
  - [Offline Mode(PWA)](pwa.md)
  - [Server-Side Rendering(SSR)](ssr.md)
  - [Embed Files](embed-files.md)

- [Awesome docsify](awesome.md)
- [Changelog](changelog.md)